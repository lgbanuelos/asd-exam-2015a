Given(/^I am a registered customer$/) do
  @customer = FactoryGirl.create(:customer, name: 'Darth Vader')
  puts(@customer.id)
end

When(/^I query my ride history$/) do
  visit '/rides/history'
  fill_in 'Start', :with => '2015-05-01'
  fill_in 'End', :with => '2015-05-31'
  first('input#query_customer_id', visible: false).set(@customer.id)
  click_on 'Query history'
end

Then(/^I should see the ride history for the last month$/) do
  pending # express the regexp above with the code you wish you had
end
