Feature: Ride statement
  As a customer
  I want to query my ride history
  So that I can apply for rewards

  Scenario: Raw ride history
    Given I am a registered customer
    When I query my ride history
    Then I should see the ride history for the last month
