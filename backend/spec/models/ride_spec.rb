require 'spec_helper'

describe Ride do
  it 'should accept numeric costs' do
    customer = create(:customer, name: 'Darth Vader')
    10.times do
      create(:ride, customer: customer)
    end
    expect(Ride.count).to be(10)
  end
end
