# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ride do
    customer nil
    cost 9.99
    ride_date "2015-06-01"
    payment "MyString"
  end
end
