class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.references :customer
      t.decimal :cost
      t.date :ride_date
      t.string :payment

      t.timestamps
    end
    add_index :rides, :customer_id
  end
end
