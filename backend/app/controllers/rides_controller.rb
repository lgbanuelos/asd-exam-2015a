class RidesController < ApplicationController
  def history_form
  end

  def query_history
    customer_id = params[:query][:customer_id]
    start_date = DateTime.parse(params[:query][:start_date])
    end_date = DateTime.parse(params[:query][:end_date])
    puts customer_id
    puts start_date
    puts end_date
    @rides = Ride.where(:customer_id => customer_id, :ride_date => start_date..end_date)
  end
end
