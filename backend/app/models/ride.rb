class Ride < ActiveRecord::Base
  belongs_to :customer
  attr_accessible :cost, :payment, :ride_date
end
